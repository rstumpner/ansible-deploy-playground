## Summary
Satus und kurze Zusammenfassung des Service Requests

## Issue
Add a new Node to a Service in the Inventory

### Facts
Environment ?

- [ ] production
- [ ] canary
- [ ] review

What is the hostname of the new Node ?

`hostname2.canary.servicename.site`

What is the IP Adress of the new Node ?

`192.168.1.1`


## Agenda
- [x] Service Request
- [ ] collect facts
- [ ] Worklog

## Notes

## Worklog
Detail documentaion 

<details>

- [ ] Create Merge Request 
- [ ] Update Repository inventory/all.yml 

    ```
servicename:
   children:
        servicename-production:
            hosts:
                hostname.prod.servicename.site.region:
                    ansible_host:
        servicename-canary:
            hosts:
                hostname.canary.servicename.site.region:
                    ansible_host:
                hostname2.canary.servicename.site.region:
                    ansible_host:
        servicename-review:
            hosts:
                hostname.review.servicename.site.region:
                    ansible_connection: local
    ```

- [ ] Select Reviewer
- [ ] Mark as Ready
- [ ] Merge to Master
</details>




/label ~"service::request" 
/weight 1