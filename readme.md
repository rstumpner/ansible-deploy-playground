# Ansible Repository for a Deployment Playground

This Ansible Repository contains a Deployment Framework from a Template (https://gitlab.com/rstumpner/ansible-deploy-template) with a working CI/CD Pipeline in Gitlab. This base is used for Integration Tests of Ansible Roles and Deployment Experiments.


## Feature Integrated CI/CD Pipeline 
Stages with Ansible tags for execution included:
- validate
- build
- static_tests
- prepare
- deploy

#### Validate:
Validates the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --syntax-check -vvvv`

#### Build:
Builds the configuration files from the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --tags build --vault-password-file /tmp/vault-password`

#### Inventory:
Some words about the Inventory

#### Environments:
Some words about the Environments that are tested


## Feature Artifacts

Artifacts are collected on all stages . On the different stages there may be different artifacts like config files on the build stage and some reports on the deployment stage.

Artifacts path:

| path | description | Ansible tag|
|--- | --- |--- | 
| build | some artifacts from the build process (configs/packages)| [build]|
| test | Files or Reports from the Testing stage | [build] |
| state | some artifacts from the deployment process (diffs/reports) | [deploy] |
| docs | some artifacts from the generated documentation | [build,docs]|

 
License:
    MIT / BSD

Author Information:
roland@stumpner.at
